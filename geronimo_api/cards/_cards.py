# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: _cards.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """
from __future__ import annotations

from abc import ABC
from typing import Union

import numpy as np

from geronimo_api.drivers import Driver
from geronimo_api.registers import Register


class Cards(ABC):

    @classmethod
    def get_by_name(cls, name: str) -> Union[Register, None]:
        try:
            return cls.__dict__[name.upper()]
        except KeyError:
            try:
                return eval(f"cls.__dict__[{name.lower().replace('-', '_').upper()}]")
            except AttributeError:
                return None

    @classmethod
    def get_by_name_channel(cls, name: str, channel: int) -> Union[Register, None]:
        try:
            return cls.__dict__[f"{name.upper()}_CH{channel:02d}"]
        except KeyError:
            return None

    @classmethod
    def read_register(cls, register: int, driver: Driver):
        return hex(np.uint32(np.int32(driver.read_register(register))))[2:].upper()

    @classmethod
    def write_register(cls, register: int, value: int, driver: Driver):
        return driver.write_register(register, value)
