# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: hf8ch.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import numpy as np
import time

from math import ceil
from typing import Dict, List, Union

from geronimo_api.cards._cards import Cards
from geronimo_api.drivers import Driver
from geronimo_api.enums import Hf8Ch_Acq, Hf8Ch_Ch, Hf8Ch_Gain, Hf8Ch_Fs, Hf8Ch_FsEm
from geronimo_api.miscs import GeronimoExceptionHf8Ch
from geronimo_api.registers import *


class Hf8Ch(Cards):
    TRIGGER_SOFT: Register = RegisterBoolean("trigger-soft", 0, RegisterMode.READ_WRITE, 0)
    RESET_SOFT: Register = RegisterBoolean("reset-soft", 0, RegisterMode.READ_WRITE, 1)
    CONFIG_SOFT: Register = RegisterBoolean("config-soft", 0, RegisterMode.READ_WRITE, 2)
    CLEAR_INT_DMA: Register = RegisterBoolean("clear-int-dma", 0, RegisterMode.READ_WRITE, 3)
    ENABLE_TRIGGER_ON_PPS: Register = RegisterBoolean("enable-trigger-on-pps", 0, RegisterMode.READ_WRITE, 4)
    ACTIVATE_SHOT_ON_PPS: Register = RegisterBoolean("activate-shot-on-pps", 0, RegisterMode.READ_WRITE, 5)
    ENABLE_TRIGGER_ON_EXT: Register = RegisterBoolean("enable-trigger-on-ext", 0, RegisterMode.READ_WRITE, 6)
    TRIGGER_COPY: Register = RegisterBoolean("trigger-copy", 0, RegisterMode.READ_WRITE, 7)
    POWERDOWN: Register = RegisterBoolean("powerdown", 0, RegisterMode.READ_WRITE, 8)
    PPS_2: Register = RegisterBoolean("pps-2", 0, RegisterMode.READ_WRITE, 9)
    RAMP_MODE: Register = RegisterBoolean("ramp-mode", 0, RegisterMode.READ_WRITE, 15)

    INT_DMA: Register = RegisterBoolean("int-dma", 0, RegisterMode.READ, 16)
    AWAIT_TRIGGER_EXT = RegisterBoolean("await-trigger-ext", 0, RegisterMode.READ, 17)
    AWAIT_TRIGGER_DATE = RegisterBoolean("await-trigger-date", 0, RegisterMode.READ, 18)
    CONFIG_DONE = RegisterBoolean("config-done", 0, RegisterMode.READ, 19)
    DDR_OK = RegisterBoolean("ddr-ok", 0, RegisterMode.READ, 20)
    TRIGGER_ARMED = RegisterBoolean("trigger-armed", 0, RegisterMode.READ, 21)
    ACQ_IN_PROGRESS = RegisterBoolean("acq-in-progress", 0, RegisterMode.READ, 22)

    # INT_PPS: Register = RegisterBoolean("int-pps", 0, RegisterMode.READ, 17)
    BITSTREAM: Register = RegisterIntSub("bitstream", 0, begin=24, end=32)

    DELAY: Register = RegisterTime("delay", 1, RegisterMode.READ_WRITE, 10e-9)
    SHOT_DELAY: Register = RegisterTime("shot-delay", 2, RegisterMode.READ_WRITE, 10e-9)  # Inverse de la prf
    MEANS: Register = RegisterInt("means", 3, RegisterMode.READ_WRITE, range(0, 4096))

    TX_SAMPLES: Register = RegisterInt("tx-sample", 4, RegisterMode.READ_WRITE, range(1, 65535))  # 1 - 65535
    RX_SAMPLES: Register = RegisterInt("rx-sample", 5, RegisterMode.READ_WRITE, range(0, 2 ** 29))  # Limite 2^32 // 8
    RX_SAMPLES_BYTES: Register = RegisterInt("rx-sample-bytes", 6, RegisterMode.READ_WRITE, range(0, 2 ** 32))

    GAIN_CH00: Register = RegisterEnum("gain-ch0", 7, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH01: Register = RegisterEnum("gain-ch1", 8, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH02: Register = RegisterEnum("gain-ch2", 9, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH03: Register = RegisterEnum("gain-ch3", 10, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH04: Register = RegisterEnum("gain-ch4", 11, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH05: Register = RegisterEnum("gain-ch5", 12, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH06: Register = RegisterEnum("gain-ch6", 13, RegisterMode.READ_WRITE, Hf8Ch_Gain)
    GAIN_CH07: Register = RegisterEnum("gain-ch7", 14, RegisterMode.READ_WRITE, Hf8Ch_Gain)

    TX_ACTIVE_CHANNEL: Register = RegisterEnum("tx-active-channel", 15, RegisterMode.READ_WRITE, Hf8Ch_Ch)
    BURST_DATA_RX: Register = RegisterDatas("burst-data-rx", 16, RegisterMode.READ_DATAS)
    SAMPLE_RX: Register = RegisterEnum("sample-rx", 17, RegisterMode.READ_WRITE, Hf8Ch_Fs)

    SECOND_UTC: Register = RegisterTimestamp("second-utc", 18, RegisterMode.READ)
    QERR_ON_PPS: Register = None
    CLK_COUNT_ON_PPS: Register = RegisterInt("clk-count-on-pps", 21, RegisterMode.READ)
    FS_CLK_COUNT: Register = RegisterInt("fs-clk-count", 22, RegisterMode.READ)
    FS_TIMESTAMP_UTC: Register = RegisterTimestamp("fs-timestamp-utc", 23, RegisterMode.READ)
    LS_CLK_COUNT: Register = RegisterInt("Ls-clk-count", 25, RegisterMode.READ)
    LS_TIMESTAMP_UTC: Register = RegisterTimestamp("ls-timestamp-utc", 26, RegisterMode.READ)
    DATE_TIMESTAMP_UTC: Register = RegisterTimestamp("date-timestamp-utc", 28, RegisterMode.READ_WRITE)
    AMPLIFIER_TX: Register = None
    BURST_DATA1_TX: Register = RegisterSignal("burst-data1-tx", 31, RegisterMode.WRITE_SIGNAL)
    BURST_DATA2_TX: Register = RegisterSignal("burst-data2-tx", 32, RegisterMode.WRITE_SIGNAL)

    ENABLE_THRESHOLD: Register = RegisterBoolean("enable-threshold", 33, RegisterMode.READ_WRITE, 0)
    ENABLE_THRESHOLD_CH00: Register = RegisterBoolean("enable-threshold-ch0", 33, RegisterMode.READ_WRITE, 8)
    ENABLE_THRESHOLD_CH01: Register = RegisterBoolean("enable-threshold-ch1", 33, RegisterMode.READ_WRITE, 9)
    ENABLE_THRESHOLD_CH02: Register = RegisterBoolean("enable-threshold-ch2", 33, RegisterMode.READ_WRITE, 10)
    ENABLE_THRESHOLD_CH03: Register = RegisterBoolean("enable-threshold-ch3", 33, RegisterMode.READ_WRITE, 11)
    ENABLE_THRESHOLD_CH04: Register = RegisterBoolean("enable-threshold-ch4", 33, RegisterMode.READ_WRITE, 12)
    ENABLE_THRESHOLD_CH05: Register = RegisterBoolean("enable-threshold-ch5", 33, RegisterMode.READ_WRITE, 13)
    ENABLE_THRESHOLD_CH06: Register = RegisterBoolean("enable-threshold-ch6", 33, RegisterMode.READ_WRITE, 14)
    ENABLE_THRESHOLD_CH07: Register = RegisterBoolean("enable-threshold-ch7", 33, RegisterMode.READ_WRITE, 15)

    BLINK_LED: Register = None
    RESYNC_PPS: Register = None

    ACQ_DELAY: Register = None

    LED_1: Register = RegisterBoolean("led-1", 37, RegisterMode.READ_WRITE, 0)
    LED_2: Register = RegisterBoolean("led-2", 37, RegisterMode.READ_WRITE, 1)
    LED_3: Register = RegisterBoolean("led-3", 37, RegisterMode.READ_WRITE, 2)
    LED_4: Register = RegisterBoolean("led-4", 37, RegisterMode.READ_WRITE, 3)
    LED_5: Register = RegisterBoolean("led-5", 37, RegisterMode.READ_WRITE, 4)

    THRESHOLD_POS: Register = RegisterInt("threshold-pos", 38, RegisterMode.READ_WRITE)
    THRESHOLD_NEG: Register = RegisterInt("threshold-neg", 39, RegisterMode.READ_WRITE)
    THRESHOLD_PRE_TRIG: Register = RegisterInt("threshold-pre-trig", 40, RegisterMode.READ_WRITE)

    ENABLE_CH00: Register = RegisterBoolean("enable-ch0", 41, RegisterMode.READ_WRITE, 0)
    ENABLE_CH01: Register = RegisterBoolean("enable-ch1", 41, RegisterMode.READ_WRITE, 1)
    ENABLE_CH02: Register = RegisterBoolean("enable-ch2", 41, RegisterMode.READ_WRITE, 2)
    ENABLE_CH03: Register = RegisterBoolean("enable-ch3", 41, RegisterMode.READ_WRITE, 3)
    ENABLE_CH04: Register = RegisterBoolean("enable-ch4", 41, RegisterMode.READ_WRITE, 4)
    ENABLE_CH05: Register = RegisterBoolean("enable-ch5", 41, RegisterMode.READ_WRITE, 5)
    ENABLE_CH06: Register = RegisterBoolean("enable-ch6", 41, RegisterMode.READ_WRITE, 6)
    ENABLE_CH07: Register = RegisterBoolean("enable-ch7", 41, RegisterMode.READ_WRITE, 7)

    TX_SAMPLES_FREQUENCY: Register = RegisterEnum("tx-samples-frequency", 42, RegisterMode.READ_WRITE, Hf8Ch_FsEm)

    @classmethod
    def configure_shot(cls,
                       acquisition_mode: Hf8Ch_Acq = Hf8Ch_Acq.TRIG_SOFT,
                       mean: int = 0,
                       samples: int = 1000,
                       prf: float = 1,
                       delay: float = 0,
                       sampling_frequency: Hf8Ch_Fs = Hf8Ch_Fs.FS_2MHz,
                       active_channel: Hf8Ch_Ch = Hf8Ch_Ch.CH_NONE,
                       gains: Dict[Hf8Ch_Ch, Hf8Ch_Gain] = None,
                       signal: np.ndarray = None,
                       sampling_frequency_emission: Hf8Ch_Fs = Hf8Ch_FsEm.FS_10MHz,
                       amplitude: int = 100,
                       trigger_timestamp: int = None,
                       threshold: "list[Union[float, None]]" = None,
                       driver: Driver = None):
        if driver is None:
            driver = Driver()

        if gains is None or len([key for key in gains.keys() if gains.get(key) != "GAIN_OFF"]) == 0:
            driver.logger.info("No active channel in reception!")

        active_channels = cls.set_gains(gains=gains, driver=driver)
        cls.TX_ACTIVE_CHANNEL.set(driver, active_channel)
        cls.CONFIG_SOFT.set_bit(driver, True)

        if signal is not None:
            cls.TX_SAMPLES_FREQUENCY.set(driver=driver, value=sampling_frequency_emission)
            cls.write_arb_signal(signal=signal, amplitude=amplitude, driver=driver)

        cls.DELAY.set(driver=driver, value=delay)
        cls.MEANS.set(driver=driver, value=mean)
        cls.set_prf(value=prf, driver=driver)
        cls.set_samples(samples=samples, amount_channels=len(active_channels), driver=driver)

        cls.SAMPLE_RX.set(driver, sampling_frequency)

        cls.DATE_TIMESTAMP_UTC.set(driver,
                                   trigger_timestamp if trigger_timestamp is not None else int(time.time() + 10))

        if threshold is None:
            cls.ENABLE_THRESHOLD.set_bit(driver, False)
        else:
            if threshold is not None:
                if type(threshold) is not list:
                    raise GeronimoExceptionHf8Ch("Threshold format is invalid, list is required!")
                if len(threshold) != 8:
                    raise GeronimoExceptionHf8Ch("Threshold length is invalid!")

                for ind in range(8):
                    if threshold[ind] is not None:
                        cls.get_by_name_channel("ENABLE_THRESHOLD", ind).set_bit(driver, True)
                        cls.THRESHOLD_POS.set(driver, threshold[ind])
                        cls.THRESHOLD_NEG.set(driver, -1 * threshold[ind])
                    else:
                        cls.get_by_name_channel("ENABLE_THRESHOLD", ind).set_bit(driver, False)

        val = False
        attempt = 0
        while not val:
            cls.set_acquisition_mode(mode=acquisition_mode, driver=driver)
            if acquisition_mode == Hf8Ch_Acq.TRIG_SOFT:
                cls.TRIGGER_SOFT.set_bit(driver, True)
            val = (cls.TRIGGER_ARMED.get_bit(driver=driver) or
                   cls.AWAIT_TRIGGER_DATE.get_bit(driver=driver) or
                   cls.AWAIT_TRIGGER_EXT.get_bit(driver=driver))
            attempt += 1
            time.sleep(0.01)
            if attempt > 5:
                break
        return active_channels

    @classmethod
    def round_robin_acquisition(cls,
                                acquisition_mode: Hf8Ch_Acq = Hf8Ch_Acq.TRIG_SOFT,
                                mean: int = 0,
                                samples: int = 1000,
                                prf: float = 1,
                                delay: float = 0,
                                channels: list = None,
                                sampling_frequency: Hf8Ch_Fs = Hf8Ch_Fs.FS_2MHz,
                                gain: Hf8Ch_Gain = Hf8Ch_Gain.GAIN_00dB,
                                signal: np.ndarray = None,
                                amplitude: int = 100,
                                trigger_timestamp: int = None,
                                threshold: "list[Union[float, None]]" = None,
                                raw: bool = False,
                                driver: Driver = None):
        if driver is None:
            driver = Driver()

        if signal is not None:
            cls.write_arb_signal(signal, amplitude, driver=driver)

        if channels is None:
            channels = list(Hf8Ch_Ch)[1:]

        gains = {}
        for channel in channels:
            gains.update({channel: gain})

        datas = np.empty((len(channels), len(channels), samples))
        for ind, channel in enumerate(channels):
            active_channels = cls.configure_shot(acquisition_mode=Hf8Ch_Acq.TRIG_SOFT if ind > 0 else acquisition_mode,
                                                 mean=mean,
                                                 samples=samples,
                                                 prf=prf,
                                                 delay=delay,
                                                 sampling_frequency=sampling_frequency,
                                                 active_channel=channel,
                                                 gains=gains,
                                                 signal=None,
                                                 trigger_timestamp=trigger_timestamp,
                                                 threshold=threshold,
                                                 driver=driver)
            datas[ind] = np.transpose(cls.read_datas(samples=samples,
                                                     channels=len(active_channels),
                                                     timeout=int(5 + mean / prf),
                                                     raw=raw,
                                                     driver=driver))
        return channels, datas

    @classmethod
    def set_gain(cls, channel: Hf8Ch_Ch, gain: Hf8Ch_Gain = Hf8Ch_Gain.GAIN_OFF, driver: Driver = None):
        if driver is None:
            driver = Driver()

        if channel == Hf8Ch_Ch.CH_NONE:
            return

        cls.get_by_name_channel(name="GAIN", channel=channel.get_int()).set(driver, gain)
        cls.get_by_name_channel(name="ENABLE", channel=channel.get_int()).set_bit(driver,
                                                                                  gain != Hf8Ch_Gain.GAIN_OFF)

    @classmethod
    def set_gains(cls, gains: Dict[Hf8Ch_Ch, Hf8Ch_Gain] = None, driver: Driver = None) -> List[Hf8Ch_Ch]:
        if gains is None:
            raise GeronimoExceptionHf8Ch("No active channel in reception!")

        if driver is None:
            driver = Driver()

        active_channels = []

        for channel in Hf8Ch_Ch:
            if channel == Hf8Ch_Ch.CH_NONE:
                continue
            gain = gains.get(channel, Hf8Ch_Gain.GAIN_OFF)
            if gain != Hf8Ch_Gain.GAIN_OFF:
                active_channels.append(channel)

            cls.set_gain(channel=channel, gain=gain, driver=driver)
        return active_channels

    @classmethod
    def write_arb_signal(cls, signal: np.ndarray, amplitude: int = 100, driver: Driver = None):
        max_length: int = 16384
        if len(signal) <= 0 or len(signal) > max_length:
            raise GeronimoExceptionHf8Ch(f"Invalid signal length, size must be between 1 and {max_length}!")

        if amplitude <= 0 or amplitude >= 2047:
            raise GeronimoExceptionHf8Ch("Invalid amplitude, amplitude must be between 1 and 2047!")

        if np.min(signal) < -1 or np.max(signal) > 1:
            driver.logger.info(f"[HF8CH] Write arb signal, invalid amplitude signal clip")
            signal = np.clip(signal, -1, 1)

        if signal[-1] != 0:
            driver.logger.info(f"[HF8CH] Write arb signal, last sample set to 0")
            signal[-1] = 0

        if driver is None:
            driver = Driver()

        cls.TX_SAMPLES.set(driver=driver, value=len(signal))
        signal = np.concatenate((signal, np.zeros((max_length - len(signal)))))
        signal = np.asarray(np.add(np.multiply(signal, amplitude), 2047), dtype=np.int16)
        cls.BURST_DATA1_TX.write_signal(driver, list(signal))

    @classmethod
    def set_acquisition_mode(cls, mode: Hf8Ch_Acq = Hf8Ch_Acq.TRIG_SOFT,
                             driver: Driver = None):

        if driver is None:
            driver = Driver()

        if mode == Hf8Ch_Acq.TRIG_ON_PPS:
            cls.ENABLE_TRIGGER_ON_PPS.set_bit(driver, True)
            cls.ENABLE_TRIGGER_ON_EXT.set_bit(driver, False)
            cls.ACTIVATE_SHOT_ON_PPS.set_bit(driver, True)
        elif mode == Hf8Ch_Acq.TRIG_ON_EXT:
            cls.ENABLE_TRIGGER_ON_PPS.set_bit(driver, False)
            cls.ENABLE_TRIGGER_ON_EXT.set_bit(driver, True)
            cls.ACTIVATE_SHOT_ON_PPS.set_bit(driver, False)
        else:
            cls.ENABLE_TRIGGER_ON_PPS.set_bit(driver, False)
            cls.ENABLE_TRIGGER_ON_EXT.set_bit(driver, False)
            cls.ACTIVATE_SHOT_ON_PPS.set_bit(driver, False)

    @classmethod
    def read_datas(cls, samples: int, channels: int, timeout: int = None, raw: bool = False, driver: Driver = None) -> np.ndarray:
        if driver is None:
            driver = Driver()

        if not timeout:
            timeout = 864000

        stop_time = int(time.time() + timeout)

        samples_ = ceil(samples / 32) * 32
        amount = samples_ * channels
        datas = np.zeros((samples_, channels), dtype=np.int16)
        if amount > 0:
            data = []

            run: bool = True
            while run:
                if cls.INT_DMA.get_bit(driver):
                    while amount > 20e6:
                        data.extend(cls.BURST_DATA_RX.get_datas(driver, amount=20000000, timeout=timeout))
                        amount -= 20000000
                    data.extend(cls.BURST_DATA_RX.get_datas(driver, amount=amount, timeout=timeout))
                    run = False
                else:
                    time.sleep(0.005)
                run = run and time.time() < stop_time

            for ind in range(channels):
                datas[:, ind] = data[ind::channels]

            cls.TX_ACTIVE_CHANNEL.set(driver, Hf8Ch_Ch.CH_NONE)
            cls.CONFIG_SOFT.set_bit(driver, True)
            if raw:
                return datas[:samples, :]
            else:
                return np.multiply(datas[:samples, :], 5 / 2 ** 15)
        else:
            return datas[:samples, :]

    @classmethod
    def set_prf(cls, value: float, driver: Driver = None):
        if driver is None:
            driver = Driver()
        cls.SHOT_DELAY.set(driver, 1 / value)

    @classmethod
    def set_samples(cls, samples: int, amount_channels: int = 8, driver: Driver = None):
        if driver is None:
            driver = Driver()

        samples_ = ceil(samples / 32) * 32
        cls.RX_SAMPLES.set(driver=driver, value=samples_)
        cls.RX_SAMPLES_BYTES.set(driver=driver, value=samples_ * amount_channels * 2)
