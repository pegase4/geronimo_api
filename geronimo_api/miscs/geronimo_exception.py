# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: geronimo_exception.py
# Version: 1.0
# Status: DEV
# Python: 3.9
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 19/04/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

class GeronimoException(Exception):
    pass


class GeronimoExceptionHf8Ch(GeronimoException):

    def __init__(self, message: str):
        super().__init__(message)
        self.message = message

    def __str__(self):
        return f"[Hf8Ch] {self.message}"
