# coding: utf-8
# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: libpegase_driver.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import ctypes
import gc
import numpy as np

from geronimo_api.drivers.abstract_driver import AbstractDriver
from geronimo_api.miscs.logger import GeronimoLogger


class LibPegaseDriver(AbstractDriver):

    def __init__(self, path: str, debug: bool = False, logger: GeronimoLogger = None) -> None:
        super().__init__(path=path, debug=debug, logger=logger)
        self.lib = None
        self.driver = None

    def load(self) -> None:
        self.lib = ctypes.CDLL(self.path)
        self.driver = self.lib.create_hf8ch_basic_fnc()
        _ = self.lib.init(self.driver)

        self.lib.secure_write_registre.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int)
        self.lib.read_registre.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_long))
        self.lib.write_registre.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.POINTER(ctypes.c_long))
        self.lib.set_bit_registre.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int, ctypes.c_bool)
        self.lib.get_bit_registre.argtypes = (ctypes.c_int, ctypes.c_int, ctypes.c_int)
        self.lib.read_data.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_int16), ctypes.c_int, ctypes.c_int)
        self.lib.write_signal.argtypes = (ctypes.c_int, ctypes.POINTER(ctypes.c_uint16), ctypes.c_int16)

    def close(self) -> None:
        # To be corrected but not serious for the moment
        del self.lib
        del self.driver
        gc.collect()

    def __enter__(self) -> AbstractDriver:
        self.load()
        return self

    def __exit__(self, exc_type, exc_val, exc_tb) -> None:
        self.close()

    def read_register(self, address: int) -> int:
        value = ctypes.c_int(0)
        result = self.lib.read_registre(self.driver, address, value)
        self.logger.debug(f"[PegaseDriver] Read register {address} - {value} [{result}]")
        return value.value

    def write_register(self, address: int, value: int) -> int:
        value = ctypes.c_int(value)
        result = self.lib.write_registre(self.driver, address, value)
        self.logger.debug(f"[PegaseDriver] Write register {address} - {value} [{result}]")
        return result

    def secure_write_register(self, address: int, value: int) -> int:
        value = ctypes.c_int(value)
        result = self.lib.secure_write_registre(self.driver, address, value)
        self.logger.debug(f"[PegaseDriver] Secure write register {address} - {value} [{result}]")
        return result

    def read_bit_register(self, address: int, bit: int) -> bool:
        value = ctypes.c_bool(True)
        result = self.lib.get_bit_registre(self.driver, address, bit)
        self.logger.debug(f"[PegaseDriver] Read bit {address}.{bit} - {value} [{result}]")
        return bool(value.value)

    def write_bit_register(self, address: int, bit: int, value: bool) -> int:
        value = ctypes.c_bool(value)
        result = self.lib.set_bit_registre(self.driver, address, bit, value)
        self.logger.debug(f"[PegaseDriver] Write bit {address}.{bit} - {value} [{result}]")
        return result

    def get_datas(self, amount: int = 1, timeout: int = -1) -> np.ndarray:
        # Limited to 40Mo per reading
        buffer = (ctypes.c_int16 * amount)()
        result = self.lib.read_data(self.driver, buffer, amount, timeout)
        self.logger.debug(f"[PegaseDriver] Read data [{result}]")
        return np.ctypeslib.as_array(buffer)

    def write_signal(self, signal: list) -> int:
        buffer = (ctypes.c_uint16 * len(signal))(*signal)
        result = self.lib.write_signal(self.driver, buffer, len(signal))
        self.logger.debug(f"[PegaseDriver] Write signal [{result}]")
        return result
