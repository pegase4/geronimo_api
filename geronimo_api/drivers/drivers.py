# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: drivers.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from enum import Enum
from typing import Type

from geronimo_api.drivers.abstract_driver import AbstractDriver
from geronimo_api.drivers.childs.libpegase_driver import LibPegaseDriver


class Drivers(Enum):

    LIB_SDK_P3 = r"/usr/lib/arm-linux-gnueabihf/libpegase.so.1.0.0", LibPegaseDriver
    LIB_SDK_P4 = r"/usr/lib/libpegase.so.1.0.0", LibPegaseDriver

    def __init__(self, path: str, driver_class: Type[AbstractDriver]):
        self.path: str = path
        self.driver_class: Type[AbstractDriver] = driver_class
