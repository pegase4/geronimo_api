# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: driver.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import logging
import numpy as np
import os
import time

from typing import Union

from geronimo_api.drivers.abstract_driver import AbstractDriver
from geronimo_api.drivers.drivers import Drivers
from geronimo_api.miscs.logger import GeronimoLogger


class Driver:

    def __init__(self, debug: bool = False):
        self.__driver: Union[AbstractDriver, None] = None
        self.logger: GeronimoLogger = GeronimoLogger(name="geronimo-api",
                                                     level=logging.DEBUG if debug else logging.INFO)

        for driver in Drivers:
            if os.path.exists(driver.path):
                self.logger.info(f"Loading driver: {driver.name}")
                self.__driver = driver.driver_class(path=driver.path, debug=debug, logger=self.logger)
                break

        if self.__driver:
            self.__driver.load()
        else:
            raise Exception(f"No driver available!")

        self.lock: bool = False

    def __wait(self):
        attempt = 0
        while self.lock and attempt < 3600000:  # Attente max d'une heure pour prendre la main sur le driver
            time.sleep(0.001)
            attempt += 1
        return not self.lock and attempt < 3600000

    def read_register(self, address: int) -> int:
        if self.__wait():
            self.lock = True
            result = self.__driver.read_register(address)
            self.lock = False
            return result

    def write_register(self, address: int, value: int) -> int:
        if self.__wait():
            self.lock = True
            result = self.__driver.write_register(address, value)
            self.lock = False
            return result

    def secure_write_register(self, address: int, value: int) -> int:
        if self.__wait():
            self.lock = True
            result = self.__driver.secure_write_register(address, value)
            self.lock = False
            return result

    def read_bit_register(self, address: int, bit: int) -> bool:
        if self.__wait():
            self.lock = True
            result = self.__driver.read_bit_register(address, bit)
            self.lock = False
            return result

    def write_bit_register(self, address: int, bit: int, value: bool) -> int:
        if self.__wait():
            self.lock = True
            result = self.__driver.write_bit_register(address, bit, value)
            self.lock = False
            return result

    def get_datas(self, amount: int = 1, timeout: int = -1) -> np.ndarray:
        if self.__wait():
            self.lock = True
            result = self.__driver.get_datas(amount, timeout)
            self.lock = False
            return result

    def write_signal(self, signal: list) -> int:
        if self.__wait():
            self.lock = True
            result = self.__driver.write_signal(signal)
            self.lock = False
            return result
