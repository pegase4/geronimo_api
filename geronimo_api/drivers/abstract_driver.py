# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: abstract_driver.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import numpy as np

from abc import abstractmethod, ABC

from geronimo_api.miscs.logger import GeronimoLogger


class AbstractDriver(ABC):

    def __init__(self, path: str, debug: bool = False, logger: GeronimoLogger = None):
        self.path = path
        self.debug = debug
        self.logger: GeronimoLogger = logger

    @abstractmethod
    def load(self):
        pass

    @abstractmethod
    def close(self):
        pass

    @abstractmethod
    def read_register(self, address: int) -> int:
        pass

    @abstractmethod
    def write_register(self, address: int, value: int) -> int:
        pass

    @abstractmethod
    def secure_write_register(self, address: int, value: int) -> int:
        pass

    @abstractmethod
    def read_bit_register(self, address: int, bit: int) -> bool:
        pass

    @abstractmethod
    def write_bit_register(self, address: int, bit: int, value: bool) -> int:
        pass

    @abstractmethod
    def get_datas(self, amount: int = 1, timeout: int = -1) -> np.ndarray:
        pass

    @abstractmethod
    def write_signal(self, signal: list) -> int:
        pass
