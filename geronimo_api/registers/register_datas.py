# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_datas.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import numpy as np

from geronimo_api.drivers import Driver
from geronimo_api.registers._register import Register, RegisterMode


class RegisterDatas(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode) -> None:
        super().__init__(name, address, mode)

    def get_datas(self, driver: Driver, amount: int = 1, timeout: int = 10) -> np.ndarray:
        return driver.get_datas(amount=amount, timeout=timeout)
