# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_int.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.drivers import Driver
from geronimo_api.registers._register import Register, RegisterMode, RegisterModeException


class RegisterTimestamp(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode) -> None:
        super().__init__(name, address, mode)

    def get(self, driver: Driver):
        if self.mode not in [RegisterMode.READ, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.READ)

        v_lsb = driver.read_register(self.address)
        v_msb = driver.read_register(self.address + 1)
        return int(f"{v_msb:032b}" + f"{v_lsb:032b}", 2)

    def set(self, driver: Driver, value: int):
        v = f"{value:064b}"
        driver.write_register(self.address, int(v[32:], 2))
        driver.write_register(self.address + 1, int(v[:32], 2))
