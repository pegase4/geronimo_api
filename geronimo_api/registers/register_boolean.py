# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_boolean.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.drivers import Driver
from geronimo_api.registers._register import Register, RegisterMethodException, RegisterMode


class RegisterBoolean(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode, bit: int) -> None:
        super().__init__(name, address, mode, bit)

    def set(self, driver: Driver, value: object) -> None:
        raise RegisterMethodException(self.name, "set")

    def get(self, driver: Driver) -> None:
        raise RegisterMethodException(self.name, "get")
