# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: _register.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from enum import Enum, auto
from typing import Union

import numpy as np

from geronimo_api.drivers import Driver


class RegisterMode(Enum):

    READ = auto()
    WRITE = auto()
    READ_WRITE = auto()
    READ_DATAS = auto()
    WRITE_SIGNAL = auto()


class RegisterModeException(Exception):

    def __init__(self, address: int, mode: RegisterMode) -> None:
        self.address: int = address
        self.mode: RegisterMode = mode

    def __str__(self):
        return repr(f"{self.mode.name} mode is disabled for the {self.address}-register!")


class RegisterMethodException(Exception):

    def __init__(self, name: str, method: str) -> None:
        self.name: str = name
        self.method: str = method

    def __str__(self):
        return repr(f"The {self.method} method is not implemented for the {self.name}-register!")


class RegisterValueException(Exception):

    def __init__(self, name: str, value: Union[int, str]) -> None:
        self.name: str = name
        self.value: Union[int, str] = value

    def __str__(self):
        return repr(f"The {self.value} value is not allowed for the {self.name}-register!")


class Register:

    def __init__(self, name: str, address: int, mode: RegisterMode, bit: int = None) -> None:
        self.name: str = name
        self.address: int = address
        self.mode: RegisterMode = mode
        self.bit: Union[int, None] = bit

    def encode_value(self, value) -> int:
        return value

    def decode_value(self, value: int):
        return value

    def get(self, driver: Driver) -> int:
        if self.mode not in [RegisterMode.READ, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.READ)
        v = self.decode_value(driver.read_register(self.address))
        return v

    def set(self, driver: Driver, value: object) -> int:
        if self.mode not in [RegisterMode.WRITE, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.WRITE)
        return driver.write_register(self.address, self.encode_value(value))

    def secure_set(self, driver: Driver, value: object) -> int:
        if self.mode not in [RegisterMode.WRITE, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.WRITE)
        return driver.secure_write_register(self.address, self.encode_value(value))

    def get_bit(self, driver: Driver) -> bool:
        if self.mode not in [RegisterMode.READ, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.READ)
        if self.bit is None:
            raise RegisterMethodException(self.name, "get_bit")
        return driver.read_bit_register(self.address, self.bit)

    def set_bit(self, driver: Driver, value: bool) -> int:
        if self.mode not in [RegisterMode.WRITE, RegisterMode.READ_WRITE]:
            raise RegisterModeException(self.address, RegisterMode.WRITE)
        if self.bit is None:
            raise RegisterMethodException(self.name, "set_bit")
        return driver.write_bit_register(self.address, self.bit, value)

    def get_datas(self, driver: Driver) -> np.ndarray:
        raise RegisterMethodException(self.name, "get_datas")

    def write_signal(self, driver: Driver, signal: list) -> int:
        raise RegisterMethodException(self.name, "write_signal")
