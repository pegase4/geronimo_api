# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_int.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.registers._register import Register, RegisterMode, RegisterValueException


class RegisterTime(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode,
                 interval: float) -> None:
        super().__init__(name, address, mode)
        self.interval: float = interval

    def encode_value(self, value: float) -> int:
        code: int = int(round(value / self.interval))
        if code > 2 ** 32 - 1:
            raise RegisterValueException(self.name, code)
        return code

    def decode_value(self, value: int) -> float:
        return value * self.interval
