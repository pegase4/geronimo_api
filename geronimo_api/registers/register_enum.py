# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_enum.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 06/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from enum import EnumMeta
from typing import Union

from geronimo_api.drivers import Driver
from geronimo_api.registers._register import Register, RegisterMode, RegisterValueException


class RegisterEnum(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode,
                 values: EnumMeta) -> None:
        super().__init__(name, address, mode)
        self.values: EnumMeta = values

    def encode_value(self, value: str) -> int:
        try:
            return self.values.__getitem__(value).value
        except KeyError:
            raise RegisterValueException(self.name, value)

    def decode_value(self, value: int) -> Union[EnumMeta, None]:
        return self.values(value)
