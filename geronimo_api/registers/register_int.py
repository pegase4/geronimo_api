# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_int.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.registers._register import Register, RegisterMode, RegisterValueException


class RegisterInt(Register):

    def __init__(self, name: str, address: int, mode: RegisterMode, limits: range = None) -> None:
        super().__init__(name, address, mode)
        if limits is None:
            limits = range(0, 2**16)
        self.limits: range = limits

    def encode_value(self, value: int) -> int:
        if value not in self.limits:
            raise RegisterValueException(self.name, value)
        return value
