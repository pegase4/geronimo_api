# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: register_int.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.drivers import Driver
from geronimo_api.registers._register import Register, RegisterMode


class RegisterIntSub(Register):

    def __init__(self, name: str, address: int, begin: int = 0, end: int = 0) -> None:
        super().__init__(name, address, RegisterMode.READ)
        self.begin: int = begin
        self.end: int = end

    def get(self, driver: Driver) -> int:
        r: int = super().get(driver)
        return int(f"{r:032b}"[32 - self.end:32 - self.begin], 2)
