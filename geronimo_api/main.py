# coding: utf-8
# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: main.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """
import os
import yaml

from geronimo_api.drivers.driver import Driver
from geronimo_api.server import Server

if __name__ == "__main__":

    with open(r"./config.yml") as file:
        config = yaml.load(file, Loader=yaml.SafeLoader)

    driver: Driver = Driver(debug=config.get("debug", False))

    os.makedirs("/cea/tmp/", exist_ok=True)

    processes = {}
    processes_config = config.get("processes", {})

    server = None
    flask_config = config.get("flask", {})
    if flask_config.get("run", False):
        server = Server(driver=driver)

    try:
        for process in processes.values():
            process.start()
        if server:
            server.app.run(
                host=flask_config.get("host", "127.0.0.1"),
                port=flask_config.get("port", 42420))
    finally:
        for process in processes.values():
            process.stop()
