# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: hf8ch_enums.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from __future__ import annotations

from geronimo_api.enums._geronimo_enum import GeronimoEnum


class EnumHf8ChAcquisitionModes(GeronimoEnum):

    TRIG_SOFT = 0
    TRIG_ON_PPS = 1
    TRIG_ON_EXT = 2


class EnumHf8ChChannels(GeronimoEnum):

    CH_NONE = 0b0000000000000000
    CH_00 = 0b0000000000000011
    CH_01 = 0b0000000000001100
    CH_02 = 0b0000000000110000
    CH_03 = 0b0000000011000000
    CH_04 = 0b0000001100000000
    CH_05 = 0b0000110000000000
    CH_06 = 0b0011000000000000
    CH_07 = 0b1100000000000000

    @classmethod
    def get_by_int(cls, value: int):
        try:
            return cls.get(f"CH_{value:02d}")
        except (KeyError, AttributeError) as e:
            return cls.CH_NONE

    def get_int(self):
        if self == EnumHf8ChChannels.CH_NONE:
            return None
        else:
            return int(self.name.split("_")[1])


class EnumHf8ChGains(GeronimoEnum):

    GAIN_OFF = 0x00
    GAIN_00dB = 0x01
    GAIN_20dB = 0x11
    GAIN_40dB = 0x21
    GAIN_60dB = 0x31

    def __str__(self):
        return f"{self.name.split('_')[1]}"

    @classmethod
    def get_by_value(cls, value: str):
        if value == "0dB":
            value = "00dB"
        try:
            return cls.get(f"GAIN_{value}")
        except (KeyError, AttributeError):
            return cls.GAIN_OFF


class EnumHf8ChSamplingFrequencies(GeronimoEnum):

    FS_2MHz = 0b00
    FS_1MHz = 0b01
    FS_500kHz = 0b11

    def __str__(self):
        return f"{self.name.split('_')[1]}"

    @classmethod
    def get_by_freq(cls, value: float) -> EnumHf8ChSamplingFrequencies:
        if value == 2e6:
            return cls.FS_2MHz
        elif value == 1e6:
            return cls.FS_1MHz
        elif value == 500e3:
            return cls.FS_500kHz
        else:
            return cls.FS_2MHz

    def get_freq(self) -> float:
        if self.name == "FS_2MHz":
            return 2e6
        elif self.name == "FS_1MHz":
            return 1e6
        elif self.name == "FS_500kHz":
            return 500e3
        else:
            return 2e6


class EnumHf8ChSamplingFrequenciesEmission(GeronimoEnum):

    FS_10MHz = 0b0000
    FS_5MHz = 0b0001
    FS_2M500kHz = 0b0011
    FS_2MHz = 0b0100
    FS_1M250kHz = 0b1000
    FS_1MHz = 0b1001
    FS_625kHz = 0b1111

    def __str__(self):
        return f"{self.name.split('_')[1]}"

    @classmethod
    def get_by_freq(cls, value: float) -> EnumHf8ChSamplingFrequenciesEmission:
        if value == 10e6:
            return cls.FS_10MHz
        elif value == 5e6:
            return cls.FS_5MHz
        elif value == 2.5e6:
            return cls.FS_2M500kHz
        elif value == 2e6:
            return cls.FS_2MHz
        elif value == 1.25e6:
            return cls.FS_1M250kHz
        elif value == 1e6:
            return cls.FS_1MHz
        elif value == 625e3:
            return cls.FS_625kHz
        else:
            return cls.FS_10MHz

    def get_freq(self) -> float:
        if self.name == "FS_10MHz":
            return 10e6
        elif self.name == "FS_5MHz":
            return 5e6
        elif self.name == "FS_2M500kHz":
            return 2.5e6
        elif self.name == "FS_2MHz":
            return 2e6
        elif self.name == "FS_1M250kHz":
            return 1.25e6
        elif self.name == "FS_1MHz":
            return 1e6
        elif self.name == "FS_625kHz":
            return 625e3
        else:
            return 10e6


class EnumHf8ChVoltage(GeronimoEnum):

    V_20 = 218
    V_30 = 338
    V_40 = 450
    V_50 = 563
    V_60 = 686
    V_70 = 768
    V_80 = 896
    V_90 = 1014
    V_100 = 1115

    def __str__(self):
        return f"{self.name.split('_')[1]}V"
