# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: _geronimo_enum.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from enum import Enum


class GeronimoEnum(Enum):

    @classmethod
    def _missing_(cls, value):
        return None

    @classmethod
    def __getitem__(cls, item):
        if isinstance(item, cls):
            return item
        return cls.get_by_value(item)

    @classmethod
    def get_by_value(cls, key):
        return cls.get(key)

    @classmethod
    def get(cls, key: str):
        try:
            return cls[key]
        except KeyError:
            try:
                return cls[key.upper().replace("-", "_")]
            except KeyError:
                return list(cls)[0]
