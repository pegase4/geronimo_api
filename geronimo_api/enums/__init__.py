# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: __init__.py.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChAcquisitionModes as Hf8Ch_Acq
from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChChannels as Hf8Ch_Ch
from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChGains as Hf8Ch_Gain
from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChSamplingFrequencies as Hf8Ch_Fs
from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChSamplingFrequenciesEmission as Hf8Ch_FsEm
from geronimo_api.enums.childs.hf8ch_enums import EnumHf8ChVoltage as Hf8Ch_V
