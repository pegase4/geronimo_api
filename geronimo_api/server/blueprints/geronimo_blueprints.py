# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: geronimo_blueprints.py
# Version: 1.0
# Status: DEV
# Python: 3.9
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 08/01/2025
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from flask import abort, jsonify, request

from geronimo_api.cards import Cards
from geronimo_api.miscs import GeronimoExceptionHf8Ch
from geronimo_api.registers import Register, RegisterModeException, RegisterValueException


def geronimo_blueprint(card: Cards):
    def decorator(func):
        def wrapper(*args, **kwargs):
            blueprint = func(*args, **kwargs)
            driver = kwargs.get('driver') if kwargs.__contains__('driver') else (args[1] if len(args) > 1 else None)

            @blueprint.errorhandler(GeronimoExceptionHf8Ch)
            def handle_geronimo_exception(error):
                driver.logger.error(f"An error as occured: {error}")
                response = jsonify({'error': str(error)})
                response.status_code = 500
                return response

            @blueprint.route("parameter/<parameter>", methods=["GET"])
            def get_config(parameter: str):
                arg: Register = card.get_by_name(parameter)
                if arg is None:
                    abort(404, "Unknown register!")

                try:
                    value = arg.get(driver)
                    return str(value)
                except RegisterModeException:
                    abort(400, "This register is not readable!")

            @blueprint.route("parameter/<parameter>", methods=["POST"])
            def set_config(parameter: str):
                arg: Register = card.get_by_name(parameter)
                if arg is None:
                    abort(404, "Unknow register!")

                if not request.is_json:
                    abort(400, "The request must be done in json!")

                value = request.json.get("value", None)
                if not request.is_json:
                    abort(400, "The query must contain the value field!")

                try:
                    arg.set(driver, value)
                    return "ok"
                except RegisterModeException:
                    abort(400, "This register is not writable!")
                except RegisterValueException:
                    abort(400, "This value is not allowed for this register!")

            @blueprint.route("register/<register>", methods=["GET"])
            def get_register(register: str):
                try:
                    int_register = int(register)
                    return str(card.read_register(register=int_register, driver=driver))
                except ValueError:
                    abort(404, "Register should be an integer!")

            @blueprint.route("register/<register>", methods=["POST"])
            def set_register(register: str):
                try:
                    int_register = int(register)
                    value = request.json.get("value", None)
                    if value is None:
                        abort(400, "Value could not be empty!")
                    card.write_register(register=int_register, value=value, driver=driver)
                    return "OK"
                except ValueError:
                    abort(404, "Register should be an integer!")

            return blueprint
        return wrapper
    return decorator
