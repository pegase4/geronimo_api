# coding: utf-8
# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: hf8ch.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

try:
    import h5py
except ModuleNotFoundError:
    pass

import numpy as np
import os
import struct
import traceback

from flask import Blueprint, request, abort, send_file

from geronimo_api.cards import Hf8Ch
from geronimo_api.drivers import Driver
from geronimo_api.server.blueprints.geronimo_blueprints import geronimo_blueprint
from geronimo_api.server.requests.hf8ch_request import Hh8ChRequest


@geronimo_blueprint(Hf8Ch)
def get_hf8ch_blueprint(server, driver: Driver):
    hf8ch_blueprint = Blueprint('hf8ch', __name__)

    @hf8ch_blueprint.route('on', methods=['GET'])
    def is_enable():
        return "1"

    @hf8ch_blueprint.route("configuration", methods=["POST"])
    def hf8ch_configuration():
        req = Hh8ChRequest.get_from_json(server, request.json, logger=driver.logger)

        try:
            req.channels = Hf8Ch.configure_shot(
                acquisition_mode=req.acquisition_mode,
                mean=req.mean,
                samples=req.samples,
                prf=req.prf,
                delay=req.delay,
                sampling_frequency=req.sampling_frequency,
                active_channel=req.active_channel,
                gains=req.gains,
                signal=req.signal,
                sampling_frequency_emission=req.sampling_frequency_emission,
                amplitude=req.amplitude,
                trigger_timestamp=req.trigger_timestamp,
                threshold=req.threshold,
                driver=driver)
            server.set_last_request(req)
            return "OK"
        except ValueError:
            abort(400, traceback.format_exc())

    @hf8ch_blueprint.route("round_robin", methods=["POST"])
    def hf8ch_round_robin():
        req = Hh8ChRequest.get_from_json(server, request.json, logger=driver.logger)

        _, datas = Hf8Ch.round_robin_acquisition(
            acquisition_mode=req.acquisition_mode,
            mean=req.mean,
            samples=req.samples,
            prf=req.prf,
            delay=req.delay,
            channels=req.channels,
            sampling_frequency=req.sampling_frequency,
            gain=req.gain,
            signal=req.signal, amplitude=req.amplitude,
            trigger_timestamp=req.trigger_timestamp,
            threshold=req.threshold,
            driver=driver)

        if os.path.exists(req.filename):
            os.remove(req.filename)

        try:
            with h5py.File(req.filename, "w") as file:
                file.create_dataset('signals', data=datas)
            return send_file(req.filename, as_attachment=True, download_name="data.h5")
        except NameError:
            return struct.pack(">" + "l" * 2, *datas.shape) + struct.pack(">" + "d" * np.prod(datas.shape),
                                                                          *datas.reshape(-1))

    @hf8ch_blueprint.route("datas", methods=["POST"])
    def hf8ch_datas():
        req = Hh8ChRequest.get_from_json(server=server, content=request.json, new=False, logger=driver.logger)

        datas = Hf8Ch.read_datas(samples=req.samples,
                                 channels=len(req.channels),
                                 timeout=req.timeout,
                                 raw=False,
                                 driver=driver)

        if os.path.exists(req.filename):
            os.remove(req.filename)

        try:
            with h5py.File(req.filename, "w") as file:
                file.create_dataset('signals', data=datas)
            return send_file(req.filename, as_attachment=True, download_name="data.h5")
        except NameError:
            return struct.pack(">" + "l" * 2, *datas.shape) + struct.pack(">" + "d" * np.prod(datas.shape),
                                                                          *datas.reshape(-1))

    return hf8ch_blueprint
