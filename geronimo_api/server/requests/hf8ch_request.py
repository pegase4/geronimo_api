# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: hf8ch_request.py
# Version: 1.0
# Status: DEV
# Python: 3.9
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 18/04/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

import numpy as np
import os
import time

from typing import Union

from geronimo_api.enums import Hf8Ch_Acq, Hf8Ch_Ch, Hf8Ch_Gain, Hf8Ch_Fs, Hf8Ch_FsEm
from geronimo_api.miscs import GeronimoExceptionHf8Ch
from geronimo_api.miscs.logger import GeronimoLogger
from geronimo_api.server.requests.geronimo_requests import GeronimoRequest


class Hh8ChRequest(GeronimoRequest):

    def __init__(self):
        super().__init__()
        self.uuid: Union[str, None] = None

        self.signal: Union[np.ndarray, None] = None
        self.amplitude: int = 100
        self.active_channel: Hf8Ch_Ch = Hf8Ch_Ch.CH_NONE
        self.gains: dict[Hf8Ch_Ch, Hf8Ch_Gain] = dict()
        self.sampling_frequency: Hf8Ch_Fs = Hf8Ch_Fs.FS_2MHz
        self.sampling_frequency_emission: Hf8Ch_FsEm = Hf8Ch_FsEm.FS_10MHz
        self.delay: float = 0
        self.acquisition_mode: Hf8Ch_Acq = Hf8Ch_Acq.TRIG_SOFT
        self.mean: int = 0
        self.samples: int = 1000
        self.prf: float = 1.0
        self.trigger_timestamp: Union[int, None] = None
        self.threshold: Union[list[Union[float, None]], None] = None
        self.channels: Union[list[Hf8Ch_Ch], None] = None
        self.gain: Hf8Ch_Gain = Hf8Ch_Gain.GAIN_00dB
        self.filename: str = "/cea/tmp/data.h5"
        self.timeout: float = -1

    @classmethod
    def get_from_json(cls, server, content, new: bool = True, logger: GeronimoLogger = None):
        uuid = content.get("uuid", None)
        if uuid is None:
            raise GeronimoExceptionHf8Ch("The UUID is mandatory!")

        timestamp = content.get("timestamp", None)
        if timestamp is not None:
            if abs(time.time() - timestamp) > 10000:
                if logger is not None:
                    logger.info("Setting the system time!")
                os.system(f"date +%s -s @{int(timestamp)}")

        request = server.get_last_request()
        if request is None or request.uuid != uuid:
            if new:
                request = cls()
                request.uuid = uuid
            else:
                raise GeronimoExceptionHf8Ch("The acquisition uuid is unknown!")

        if content.__contains__("signal"):
            signal_str: str = content.get("signal")
            if signal_str == "":
                request.signal = None
            else:
                try:
                    request.signal = np.asarray([float(val) for val in signal_str.split(";")])
                except ValueError:
                    raise GeronimoExceptionHf8Ch("The format of the signal is invalid! "
                                                 "Please specify the signal in the form 'val1;val2;val3...'.")

        if content.__contains__("amplitude"):
            request.amplitude = content.get("amplitude")

        if content.__contains__("active_channel"):
            try:
                request.active_channel = Hf8Ch_Ch.get_by_int(
                    int(str(content.get("active_channel")).replace("E", "")) - 1)
            except ValueError:
                raise GeronimoExceptionHf8Ch("The active channel is invalid! "
                                             "Please provide an integer between 1 and 8.")

        if content.__contains__("gains"):
            gains: list = content.get("gains", ["OFF"] * 8)
            if len(gains) != 8:
                raise GeronimoExceptionHf8Ch("Gain must be set for each channel!")
            for ind, gain in enumerate(gains):
                request.gains.update({
                    Hf8Ch_Ch.get_by_int(ind): Hf8Ch_Gain.get_by_value(gain)
                })

        if content.__contains__("sampling_frequency"):
            if content.get("sampling_frequency") not in [500e3, 1e6, 2e6]:
                raise GeronimoExceptionHf8Ch("The sampling frequency is invalid! "
                                             "Please provide a float in [500e3, 1e6, 2e6].")
            request.sampling_frequency = Hf8Ch_Fs.get_by_freq(content.get("sampling_frequency"))

        if content.__contains__("sampling_frequency_emission"):
            if content.get("sampling_frequency_emission") not in [10e6, 5e6, 2.5e6, 2e6, 1.25e6, 1e6, 625e3]:
                raise GeronimoExceptionHf8Ch("The sampling frequency is invalid! "
                                             "Please provide a float in [10e6, 5e6, 2.5e6, 2e6, 1.25e6, 1e6, 625e3].")
            request.sampling_frequency_emission = Hf8Ch_FsEm.get_by_freq(content.get("sampling_frequency_emission"))

        if content.__contains__("delay"):
            if type(content.get("delay")) not in [float, int] or content.get("delay") < 0:
                raise GeronimoExceptionHf8Ch("The delay is invalid! "
                                             "Please provide a positive float.")
            request.delay = content.get("delay")

        if content.__contains__("acquisition_mode"):
            if (type(content.get("acquisition_mode")) != str or
                    content.get("acquisition_mode") not in ["TRIG_SOFT", "TRIG_ON_PPS", "TRIG_ON_EXT"]):
                raise GeronimoExceptionHf8Ch("The acquisition_mode is invalid! "
                                             "Please provide a string in  ['TRIG_SOFT', 'TRIG_ON_PPS', 'TRIG_ON_EXT'].")
            request.acquisition_mode = Hf8Ch_Acq.get(content.get('acquisition_mode'))

        if content.__contains__("mean"):
            if type(content.get("mean")) != int or content.get("mean") < 0:
                raise GeronimoExceptionHf8Ch("The mean is invalid! "
                                             "Please provide a positive integer.")
            request.mean = content.get("mean")

        if content.__contains__("samples"):
            if type(content.get("samples")) != int or content.get("samples") < 1:
                raise GeronimoExceptionHf8Ch("The samples is invalid! "
                                             "Please provide a positive integer (>0).")
            request.samples = content.get("samples")

        if content.__contains__("prf"):
            if type(content.get("prf")) not in [int, float] or content.get("prf") < 0:
                raise GeronimoExceptionHf8Ch("The prf is invalid! "
                                             "Please provide a positive float.")
            request.prf = content.get("prf")

        if content.__contains__("trigger_timestamp"):
            if type(content.get("trigger_timestamp")) != int or content.get("trigger_timestamp") < 0:
                raise GeronimoExceptionHf8Ch("The trigger_timestamp is invalid! "
                                             "Please provide a positive integer.")
            request.trigger_timestamp = content.get("trigger_timestamp")

        if content.__contains__("threshold"):
            request.threshold = content.get("threshold")

        if content.__contains__("channels"):
            try:
                request.channels = [
                    Hf8Ch_Ch.get_by_int(int(ch - 1)) for ch in content.get("channels")
                ]
            except ValueError:
                raise GeronimoExceptionHf8Ch("The channels is invalid! "
                                             "Please provide positive integers between 1 and 8.")

        if content.__contains__("gain"):
            if type(content.get("gain")) != str or content.get("gain") not in ['0dB', '00dB', '20dB', '40dB', '60dB']:
                raise GeronimoExceptionHf8Ch("The gain is invalid! "
                                             "Please provide a string in ['00dB', '20dB', '40dB', '60dB'].")
            request.gain = Hf8Ch_Gain.get_by_value(content.get("gain"))

        if content.__contains__("filename"):
            if type(content.get("filename")) != str:
                raise GeronimoExceptionHf8Ch("The filename is invalid! "
                                             "Please provide a string.")
            request.filename = content.get("filename")

        if content.__contains__("timeout"):
            if type(content.get("timeout")) != int or content.get('timeout') < -1:
                raise GeronimoExceptionHf8Ch("The timeout is invalid! "
                                             "Please provide an integer [-1:].")
            request.timeout = content.get("timeout")

        return request

    def __str__(self):
        lines: list[str] = [f"--- Request {self.uuid} ---"]
        for key, value in self.__dict__.items():
            if key == "signal":
                lines.append(f"Signal: {value[:5] if value is not None else ''}...")
            else:
                lines.append(f"{key}: {value}")
        lines.append("---")

        return "\n".join(lines)
