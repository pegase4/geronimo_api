# coding: utf-8

# """
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Project Name: Geronimo-Api
# File Name: server.py
# Version: 1.0
# Status: DEV
# Python: 3.10
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Description:
#
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# Created By: Clément FISHER (clement.fisher@cea.fr)
#
# Modification Date: 07/02/2024
# •••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••••
# """

from flask import Flask
from typing import Union

from geronimo_api.drivers import Driver
from geronimo_api.server.blueprints.hf8ch import get_hf8ch_blueprint
from geronimo_api.server.requests.geronimo_requests import GeronimoRequest


class Server:

    def __init__(self, driver: Driver):
        self.app = Flask("Geronimo")
        self.driver = driver

        self.last_request: Union[GeronimoRequest, None] = None
        self.app.register_blueprint(get_hf8ch_blueprint(self, self.driver), url_prefix="/api/v1/hf8ch")

    def set_last_request(self, request: GeronimoRequest):
        self.last_request = request

    def get_last_request(self):
        return self.last_request
