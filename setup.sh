#! /usr/bin/env bash

find . -type f \( -name "*.sh" -o -name "*.yml" -o -name "*.py" \) -exec sed -i 's/\r//g' {} +

cd /opt/geronimo_api || exit
chmod +x ./start.sh

dpkg -s python3-setuptools >/dev/null 2>&1 && {
        echo "python3-setuptools is installed."
    } || {
        apt-get install -y python3-setuptools
    }

dpkg -s python3-numpy >/dev/null 2>&1 && {
        echo "python3-numpy is installed."
    } || {
        apt-get install -y python3-numpy
    }

dpkg -s python3-flask >/dev/null 2>&1 && {
        echo "python3-flask is installed."
    } || {
        apt-get install -y python3-flask
    }

dpkg -s python3-yaml >/dev/null 2>&1 && {
        echo "python3-yaml is installed."
    } || {
        apt-get install -y python3-yaml
    }
python3 setup.py develop


systemctl stop geronimo
cp ./geronimo.service /etc/systemd/system/geronimo.service
systemctl daemon-reload
systemctl enable geronimo
systemctl start geronimo
