#! /usr/bin/env bash

if [ ! -d "/opt/geronimo_api/geronimo_api.egg-info" ]; then
    echo "Installation du paquet";
    cd /opt/geronimo_api/
    python3 setup.py develop;
fi
cd /opt/geronimo_api/geronimo_api/ || exit
python3 -u ./main.py
