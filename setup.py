from setuptools import setup

setup(
    name='geronimo_api',
    version='1.0',
    packages=['geronimo_api.cards', 'geronimo_api.cards.childs',
              'geronimo_api.enums', 'geronimo_api.enums.childs',
              'geronimo_api.server', 'geronimo_api.server.blueprints',
              'geronimo_api.drivers', 'geronimo_api.drivers.childs', 'geronimo_api.registers'],
    url='',
    license='',
    author='[CEA] Clément Fisher',
    author_email='clement.fisher@cea.fr',
    description='',
    zip_safe=True
)
